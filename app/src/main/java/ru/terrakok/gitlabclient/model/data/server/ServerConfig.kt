package ru.terrakok.gitlabclient.model.data.server

/**
 * @author Konstantin Tskhovrebov (aka terrakok) on 23.04.17.
 */
class ServerConfig {
    val SERVER_URL = "https://gitlab.com/"
    val APP_ID = "808b7f51c6634294afd879edd75d5eaf55f1a75e7fe5bd91ca8b7140a5af639d"
    val APP_KEY = "a9dd39c8d2e781b65814007ca0f8b555d34f79b4d30c9356c38bb7ad9909c6f3"
    val AUTH_REDIRECT_URI = "app://gitlab.client/"
}